import json
import csv
import os

class Converter:
    def Update():
        listaRaskrizja = []

        if not os.path.exists("./Assets/Raskrizja.json"):
            with open("./Assets/Raskrizja.json","w") as dat:
                json.dump({},dat)

        with open("./Assets/Zagreb_sva_raskrizja.csv","r") as dat:
            raskrizjaDict = csv.DictReader(dat,delimiter=";")

            for row in raskrizjaDict:
                listaRaskrizja.append(row)

        with open("./Assets/Raskrizja.json","w") as dat:
            json.dump(listaRaskrizja,dat)


