import folium
from flask import render_template, request, redirect, url_for
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Models.User import User
import pandas as pds
import matplotlib.pyplot as plt

def addlabels(x,y):
    for i in range(len(x)):
        plt.text(i, y[i], y[i], ha = 'center')

engine = create_engine("sqlite:///Assets/users.db",connect_args={'check_same_thread':False})
DBSession = sessionmaker(bind=engine)
sesija = DBSession()

dframe = pds.read_csv("./Assets/Zagreb_sva_raskrizja.csv",delimiter=";", encoding='latin1')
dframe = dframe.replace(',','.', regex=True)

def setup(app):
    logedIn = []
    @app.route("/index")
    def index():
        if len(logedIn)>0:
            return redirect(url_for("home"))
        return render_template("login.html")

    @app.route("/login",methods = ["POST","GET"])
    def login():
        if request.method == "POST":
            username = request.form.get("usernameEntered")
            password = request.form.get("passwordEntered")

            reqUser = sesija.query(User).filter(User.username == username)
            if len(list(reqUser))>0:
                reqUser = reqUser[0]
                if reqUser.password == password:
                    logedIn.append(reqUser)
                    return redirect(url_for("home"))
                else:
                    return redirect(url_for("index"))
            else:
                return redirect(url_for("index"))
        else:
            return redirect(url_for("index"))

    @app.route("/home")
    def home():
        if len(logedIn) == 0:
            return redirect(url_for("index"))

        map = folium.Map(location = [45.811745895605426, 15.977766186993069],zoom_start=11)

        for index, row in dframe.iterrows():
            if row["Tvrtka"] == "Swarco Croatia d.o.o.":
                ikona = folium.CustomIcon("./Assets/swarco.png",icon_size=(14, 14))
            elif row["Tvrtka"] == "Semafor d.o.o.":
                ikona = folium.CustomIcon("./Assets/semafor.png",icon_size=(14, 14))
            elif row["Tvrtka"] == "Fanos":
                ikona = folium.CustomIcon("./Assets/fanos.png",icon_size=(14, 14))
            elif row["Tvrtka"] == "Nikola Tesla":
                ikona = folium.CustomIcon("./Assets/tesla.png",icon_size=(14, 14))
            elif row["Tvrtka"] == "Traf signal d.o.o.":
                ikona = folium.CustomIcon("./Assets/traf.png",icon_size=(14, 14))
            else:
                ikona = folium.CustomIcon("./Assets/ostalo.png",icon_size=(14, 14))
            popup = folium.Popup(str(row['Kod_raskri']) + "<br>" + row["Name"] + "<br>" + row["Tvrtka"],max_width=200)
            folium.Marker(location=[row["Y"], row["X"]],popup=popup,icon=ikona).add_to(map)
        
                
        legend_html = '''
        <div style="position: fixed; top: 10px; right: 10px; background-color: white; z-index:9999; padding: 10px; border-radius: 5px; border: 2px solid grey;">
            <div style="display: flex; align-items: center;">
                <img src="/Assets/swarco.png" style="height: 20px; width: 20px; margin-right: 10px;"></img>
                <div>Swarco</div>
            </div>
            <div style="display: flex; align-items: center;">
                <img src="/Assets/semafor.png" style="height: 20px; width: 20px; margin-right: 10px;"></img>
                <div>Semafor</div>
            </div>
            <div style="display: flex; align-items: center;">
                <img src="/Assets/fanos.png" style="height: 20px; width: 20px; margin-right: 10px;"></img>
                <div>Fanos</div>
            </div>
            <div style="display: flex; align-items: center;">
                <img src="/Assets/tesla.png" style="height: 20px; width: 20px; margin-right: 10px;"></img>
                <div>Nikola Tesla</div>
            </div>
            <div style="display: flex; align-items: center;">
                <img src="/Assets/traf.png" style="height: 20px; width: 20px; margin-right: 10px;"></img>
                <div>Traf signal d.o.o.</div>
            </div>
            <div style="display: flex; align-items: center;">
                <img src="/Assets/ostalo.png" style="height: 20px; width: 20px; margin-right: 10px;"></img>
                <div>Ostali</div>
            </div>
        </div>
        '''
        map.get_root().html.add_child(folium.Element(legend_html))

        return render_template("map.html", map=map._repr_html_())

    @app.route("/statistics")
    def statistics():
        if len(logedIn) == 0:
            return redirect(url_for("index"))

        
        numberSwarco = len(dframe.query("Tvrtka =='Swarco Croatia d.o.o.'"))
        numberSemafor = len(dframe.query("Tvrtka =='Semafor d.o.o.'"))
        numberFanos = len(dframe.query("Tvrtka =='Fanos'"))
        numberTesla = len(dframe.query("Tvrtka =='Nikola Tesla'"))
        numberTraf = len(dframe.query("Tvrtka =='Traf signal d.o.o.'"))
        numberOstali = len(dframe.query("Tvrtka =='Ostali'"))
        names = ["Swarco Croatia d.o.o.",'Semafor d.o.o.','Fanos','Nikola Tesla','Traf signal d.o.o.','Ostali']
        
        plt.figure(figsize = (11, 5))
        plt.bar(names,[numberSwarco,numberSemafor,numberFanos,numberTesla,numberTraf,numberOstali])
        addlabels(names,[numberSwarco,numberSemafor,numberFanos,numberTesla,numberTraf,numberOstali])
        plt.savefig("./Assets/PerConstructor.png")

        numberEc1 = len(dframe.query("Tip_uredaj =='EC - 1'"))
        numberEc2c = len(dframe.query("Tip_uredaj =='EC - 2/C'"))
        numberEc2n = len(dframe.query("Tip_uredaj =='EC - 2/N'"))
        numberSrtc = len(dframe.query("Tip_uredaj =='SRTC-6'"))
        numberMske = len(dframe.query("Tip_uredaj =='MSKE 60'"))
        numberIskra = len(dframe.query("Tip_uredaj =='ISKRA'"))
        numberFan = len(dframe.query("Tip_uredaj =='FAN 2000'"))
        numberJcf = len(dframe.query("Tip_uredaj =='JCF 1203'"))
        numberJcfba = len(dframe.query("Tip_uredaj =='JCFBA300'"))
        numberLm = len(dframe.query("Tip_uredaj =='LM 8000'"))
        numberPsc = len(dframe.query("Tip_uredaj =='PSC'"))
        numberPsv = len(dframe.query("Tip_uredaj =='PSV'"))
        numberSiemens = len(dframe.query("Tip_uredaj =='Siemens'"))
        numberTs = len(dframe.query("Tip_uredaj =='TS T-1'"))
        products = ["EC - 1",'EC - 2/C','EC - 2/N','SRTC-6','MSKE 60.','ISKRA','FAN 2000','JCF 1203','JCFBA300','LM 8000','PSC','PSV','Siemens','TS T-1']

        plt.figure(figsize = (13, 5))
        plt.bar(products,[numberEc1,numberEc2c,numberEc2n,numberSrtc,numberMske,numberIskra,numberFan,numberJcf,numberJcfba,numberLm,numberPsc,numberPsv,numberSiemens,numberTs],width=0.5)
        addlabels(products,[numberEc1,numberEc2c,numberEc2n,numberSrtc,numberMske,numberIskra,numberFan,numberJcf,numberJcfba,numberLm,numberPsc,numberPsv,numberSiemens,numberTs])
        plt.savefig("./Assets/PerProduct.png")

        numberBrezovica = len(dframe.query("Cetvrt =='Brezovica'"))
        numberCrnomerec = len(dframe.query("Cetvrt =='Crnomerec'"))
        numberDonjaDubrava = len(dframe.query("Cetvrt =='Donja Dubrava'"))
        numberDonjiGrad = len(dframe.query("Cetvrt =='Donji grad'"))
        numberGornjaDubrava = len(dframe.query("Cetvrt =='Gornja Dubrava'"))
        numberGornjiGrad = len(dframe.query("Cetvrt =='Gornji grad - Medvescak'"))
        numberMaksimir = len(dframe.query("Cetvrt =='Maksimir'"))
        numberNZIstok = len(dframe.query("Cetvrt =='Novi Zagreb - Istok'"))
        numberNZZapad = len(dframe.query("Cetvrt =='Novi Zagreb - Zapad'"))
        numberPescenica = len(dframe.query("Cetvrt =='Pescenica - Zitnjak'"))
        numberPodsljeme = len(dframe.query("Cetvrt =='Podsljeme'"))
        numberPodsused = len(dframe.query("Cetvrt =='Podsused - Vrapce'"))
        numberSesvete = len(dframe.query("Cetvrt =='Sesvete'"))
        numberStenjevec = len(dframe.query("Cetvrt =='Stenjevec'"))
        numberTresnjevkaJ = len(dframe.query("Cetvrt =='Tresnjevka jug'"))
        numberTresnjevkaS = len(dframe.query("Cetvrt =='Tresnjevka sjever'"))
        numberTrnje = len(dframe.query("Cetvrt =='Trnje'"))
        districts =['Brezovica','Crnomerec','Donja Dubrava','Donji grad','Gornja Dubrava','Gornji grad - Medvescak','Maksimir','Novi Zagreb - Istok','Novi Zagreb - Zapad','Pescenica - Zitnjak','Podsljeme','Podsused - Vrapce','Sesvete','Stenjevec','Tresnjevka jug','Tresnjevka sjever','Trnje']
        
        plt.figure(figsize = (8, 8))
        plt.pie([numberBrezovica,numberCrnomerec,numberDonjaDubrava,numberDonjiGrad,numberGornjaDubrava,numberGornjiGrad,numberMaksimir,numberNZIstok,numberNZZapad,numberPescenica,numberPodsljeme,numberPodsused,numberSesvete,numberStenjevec,numberTresnjevkaJ,numberTresnjevkaS,numberTrnje],labels=districts,autopct='%1.1f%%')
        plt.savefig("./Assets/PerDistrict.png")

        return render_template("statistics.html")
    
    @app.route("/users")
    def users():
        if len(logedIn) == 0:
            return redirect(url_for("index"))
        allUsers = sesija.query(User).all()
        return render_template("users.html",users = allUsers)
    
    @app.route("/register",methods = ["POST","GET"])
    def register():
        if len(logedIn) == 0:
            return redirect(url_for("index"))
        if request.method == "POST":
            newName = request.form.get("newUsername")
            newPassword = request.form.get("newPassword")
            newUser = User(username = newName,password = newPassword)
            sesija.add(newUser)
            sesija.commit()
            return redirect(url_for("users"))
        else:
            return redirect(url_for("users"))

    
    @app.route("/logout")
    def logout():
        if len(logedIn)==0:
            return redirect(url_for("index"))
        logedIn.pop()
        return redirect(url_for("index"))


    app.add_url_rule("/","index",index)
    app.run(host='0.0.0.0', port=5000, debug=True)